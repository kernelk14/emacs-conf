(define-package "git-commit" "20211204.2135" "Edit Git commit messages."
  '((emacs "25.1")
    (dash "20210826")
    (transient "20210920")
    (with-editor "20211001"))
  :commit "03f495f351d6e622143e4628681019df1313a5f2" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li")
    ("Sebastian Wiesner" . "lunaryorn@gmail.com")
    ("Florian Ragwitz" . "rafl@debian.org")
    ("Marius Vollmer" . "marius.vollmer@gmail.com"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("git" "tools" "vc")
  :url "https://github.com/magit/magit")
;; Local Variables:
;; no-byte-compile: t
;; End:
