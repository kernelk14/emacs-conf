;; -*- no-byte-compile: t; lexical-binding: nil -*-
(define-package "hybrid-reverse-theme" "20220921.1345"
  "Emacs theme with material color scheme."
  '((emacs "24.1"))
  :url "https://github.com/Riyyi/emacs-hybrid-reverse"
  :commit "5c60e7428d3c135c5f027d09f4474ed776f80d8d"
  :revdesc "5c60e7428d3c"
  :keywords '("faces" "theme"))
